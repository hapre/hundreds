#!/usr/bin/env python3

import os
import bs4
import json
import pprint
import statistics

import markutil

for sex in [ 'men', 'women' ]:
  tokyotimes = {}
  fname = 'results/results_{}_marathon_final_result.html'.format(sex)
  athletes = json.loads(bs4.BeautifulSoup(open(fname).read(), 'html.parser').find('script', id='__NEXT_DATA__').contents[0])['props']['pageProps']['eventPhasesByDiscipline']['units'][0]['results']
  for ath in athletes:
    if ':' not in ath['resultMark']: continue
    tokyotimes[str(ath['competitorId_WA'])] = [ markutil.marktosecs(ath['resultMark']), ath['resultMark'] ]
  deltas = []
  deltnames = []
  for fname in os.listdir('athletes'):
    waid = fname.split('-')[0]
    if waid not in tokyotimes: continue
    athname = '-'.join(fname.split('-')[1:])[:-len('.html')].replace('_', ' ')
    ath = json.loads(bs4.BeautifulSoup(open('athletes/' + fname).read(), 'html.parser').find('script', id='__NEXT_DATA__').contents[0])['props']['pageProps']
    pb = [ r for r in ath['competitor']['personalBests']['results'] if r['disciplineCode'] == 'MAR' ][0]
    pbsecs = markutil.marktosecs(pb['mark'])
    delt = tokyotimes[waid][0] - pbsecs
    deltas.append(delt)
    deltnames.append((delt, athname, ath['competitor']['basicData']['countryCode'], pb['mark'], tokyotimes[waid][1]))
  for i, ath in enumerate(sorted(deltnames)):
    print('{}. {} ({}) {}s ({} - {})'.format(i+1, ath[1], ath[2], ath[0], ath[4], ath[3]))
  print(sex, 'median', statistics.median(deltas), 'mean', statistics.mean(deltas))

  
