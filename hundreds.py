#!/usr/bin/env python3

import os
import bs4
import csv
import json
import pprint
import datetime

import parsepdf
import markutil

pdfunits = parsepdf.getpdfs()

with open('splits.csv', 'w') as csvfile:
  writer = csv.DictWriter(csvfile, fieldnames = ['event', 'sex', 'phase', 'heatNum', 'athlete', 'country', 'splitName', 'splitNum', 'accelStreak', 'decelStreak', 'totalSeconds', 'split'])
  writer.writeheader()
  segrecs = { 'M': {}, 'W': {}, 'X': {} }
  for fname in os.listdir('results'):
    soup = bs4.BeautifulSoup(open('results/' + fname, 'r').read(), 'html.parser').find('script', id='__NEXT_DATA__')
    if not soup: continue
    evt = json.loads(soup.contents[0])['props']['pageProps']['eventPhasesByDiscipline']
    if not evt: continue
    print(evt['discipline']['name'], evt['sexCode'], evt['phaseName'])
    if evt['discipline']['name'] not in segrecs[evt['sexCode']]: segrecs[evt['sexCode']][evt['discipline']['name']] = {}
    segs = segrecs[evt['sexCode']][evt['discipline']['name']]
    for unitIdx, unit in enumerate(evt['units']):
      unitkey = (evt['discipline']['name'], evt['sexCode'], evt['phaseName'], unitIdx + 1)
      if unitkey in pdfunits: unit = pdfunits[unitkey]
      athletes = {}
      if not unit['splits']: continue
      lastsplit = []
      for result in unit['results']:
        if ':' not in result['resultMark']: continue
        result['intermediateMark'] = result['resultMark'] + 'h'
        result['competitionIntermediateName'] = evt['discipline']['name'] + ' (final split)'
        lastsplit.append(result)
      unit['splits'].insert(0, lastsplit)
      for split in reversed(unit['splits']):
        split_name = split[0]['competitionIntermediateName']
        if split_name not in segs: segs[split_name] = { 'split': float('inf') }
        for athlete in split:
          athname = athlete['competitorName']
          athmark = athlete['intermediateMark']
          if athmark.endswith('h'): athmark = athmark[:-1]
          athsecs = markutil.marktosecs(athmark)
          athsplit = {
            'event': evt['discipline']['name'],
            'sex': evt['sexCode'],
            'phase': evt['phaseName'],
            'splitName': split_name,
            'athlete': athlete['competitorName'],
            'totalSeconds': athsecs,
            'heatNum': unitIdx + 1,
            'country': athlete['competitorCountryName'],
            'accelStreak': 1,
            'decelStreak': 1,
          }
          if athname in athletes:
            previdx = -2 if athletes[athname][-1]['splitName'] == 'Half' else -1
            athsplit['splitNum'] = len(athletes[athname]) + 1
            athsplit['split'] = round(athsecs - athletes[athname][previdx]['totalSeconds'], 1)
            if athsplit['split'] <= athletes[athname][previdx]['split']: athsplit['accelStreak'] = athletes[athname][previdx]['accelStreak'] + 1
            if athsplit['split'] >= athletes[athname][previdx]['split']: athsplit['decelStreak'] = athletes[athname][previdx]['decelStreak'] + 1
            if athsplit['splitName'] in ['Half', 'Marathon (final split)']:
              athsplit['accelStreak'] = 0
              athsplit['decelStreak'] = 0
            athletes[athname].append(athsplit)
          else:
            athsplit['splitNum'] = 1
            athsplit['split'] = athsecs
            athletes[athname] = [ athsplit ]
          if athsplit['split'] < segs[athsplit['splitName']]['split']:
            segs[athsplit['splitName']]['split'] = athsplit['split']
            segs[athsplit['splitName']]['athletes'] = [ athsplit ]
          elif athsplit['split'] == segs[athsplit['splitName']]['split']:
            segs[athsplit['splitName']]['athletes'].append(athsplit)
          writer.writerow(athsplit)

segrecfreq = {}
for sex in segrecs:
  for evt in segrecs[sex]:
    pprint.pprint([ (key, split['split']) for key, split in segrecs[sex][evt].items() ])
    print(evt, sex, str(datetime.timedelta(seconds = round(sum(split['split'] for key, split in segrecs[sex][evt].items()), 2))))
    for split in segrecs[sex][evt]:
      for ath in segrecs[sex][evt][split]['athletes']:
        athname = ath['athlete']
        infotuple = (ath['event'], ath['phase'], ath['heatNum'], ath['splitName'], ath['split'])
        if athname in segrecfreq:
          segrecfreq[athname][0] += 1
          segrecfreq[athname][1].append(infotuple)
        else:
          segrecfreq[athname] = [ 1, [infotuple] ]

for athname, recinfo in dict(sorted(segrecfreq.items(), key = lambda item: item[1])).items():
  print(athname, recinfo[0])
  for record in recinfo[1]:
    print('  ', record)
