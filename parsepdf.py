#!/usr/bin/env python3

import bs4
import pprint

# wget https://olympics.com/tokyo-2020/olympic-games/resOG2020-/pdf/OG2020-/ATH/OG2020-_ATH_C77A_ATHM1500M-------------SFNL000200--.pdf
# pdftohtml OG2020-/ATH/OG2020-_ATH_C77A_ATHM1500M-------------SFNL000200--.pdf

pdfs = [
  {
    'fname': 'pdfs/OG2020-_ATH_C77A_ATHM1500M-------------SFNL000200--s.html',
    'splits': 14,
    'event': '1500 Metres',
    'sex': 'M',
    'phase': 'Semi-Final',
    'heatNum': 2
  }
]

def getpdfs():
  pdfunits = {}
  for pdf in pdfs:
    splits = [ [] for _ in range(pdf['splits'] - 1) ]
    athletes = []
    soup = bs4.BeautifulSoup(open(pdf['fname'], 'r').read(), 'html.parser')
    place = 1
    psoup = soup.find('b', text = '1')
    while psoup:
      athsoup = psoup.next_sibling.next_sibling.next_sibling
      athlete = {
        'competitorName': ' '.join(reversed(athsoup.text.split(' '))),
        'competitorCountryName': athsoup.next_sibling.next_sibling.strip(),
        'competitiorCountryCode': athsoup.next_sibling.next_sibling.strip(),
        'resultMark': athsoup.find_next('b').text
      }
      place += 1
      psoup = soup.find('b', text = str(place))
      athletes.append(athlete)
    times = [ c.strip() for c in soup.find('body').children if '.' in c ]
    for athi, athsplits in enumerate([ times[i:i + pdf['splits'] ] for i in range(0, len(times), pdf['splits']) ]):
      for marki, athmark in enumerate(athsplits):
        splits[athi].append({
          **athletes[athi],
          **pdf,
          'intermediateMark': athmark + 'h',
          'competitionIntermediateName': str((1+marki) * 100) + 'm'
        })
    pdfunits[(pdf['event'], pdf['sex'], pdf['phase'], pdf['heatNum'])] = {
      'splits': splits,
      'results': athletes
    }
  return pdfunits
