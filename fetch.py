#!/usr/bin/env python3

import os
import bs4
import json
import requests

PREFIX = 'https://www.worldathletics.org/competitions/olympic-games/the-xxxii-olympic-games-athletics-7132391/'
TIMETABLE = PREFIX + 'timetable'

def slug(url): return '_'.join(url.split('/')[6:]) + '.html'

if not os.path.isfile(slug(TIMETABLE)): open(slug(TIMETABLE), 'w').write(requests.get(TIMETABLE).text)

tdata = json.loads(bs4.BeautifulSoup(open(slug(TIMETABLE), 'r').read(), 'html.parser').find('script', id='__NEXT_DATA__').contents[0])
for evt in tdata['props']['pageProps']['eventTimetable']:
  if evt['phaseNameUrlSlug'] in ['decathlon', 'heptathlon']:
    url = '{}results/{}/{}/{}/result'.format(PREFIX, evt['sexNameUrlSlug'], evt['phaseNameUrlSlug'], evt['discipline']['nameUrlSlug'])
  else:
    url = '{}results/{}/{}/{}/result'.format(PREFIX, evt['sexNameUrlSlug'], evt['discipline']['nameUrlSlug'], evt['phaseNameUrlSlug'])
  if not os.path.isfile('results/' + slug(url)): open('results/{}'.format(slug(url)), 'w').write(requests.get(url).text)
  print(url)
