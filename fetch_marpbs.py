#!/usr/bin/env python3

import bs4
import json
import pprint
import requests

for fmar in [ 'results/results_men_marathon_final_result.html', 'results/results_women_marathon_final_result.html' ]:
  athletes = json.loads(bs4.BeautifulSoup(open(fmar).read(), 'html.parser').find('script', id='__NEXT_DATA__').contents[0])['props']['pageProps']['eventPhasesByDiscipline']['units'][0]['results']
  for ath in athletes:
    url = 'https://worldathletics.org/athletes/_/{}'.format(ath['competitorId_WA'])
    open('athletes/{}-{}.html'.format(ath['competitorId_WA'], ath['competitorName'].replace(' ', '_')), 'w').write(requests.get(url).text)
    print(url, ath['competitorName'])
