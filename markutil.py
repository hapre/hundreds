#!/usr/bin/env python3

def marktosecs(mark):
  while mark.count(':') < 2: mark = '0:' + mark
  h, m, s = mark.split(':')
  return int(h) * 3600 + int(m) * 60 + float(s)
